public class ValueSetRPGSample {
	const int maxBuffCount = 5;
	const int maxLevelCount = 3;

	public class CharType {
		public const int Fighter = 0;
		public const int Mage = 1;
		public const int Count = 2;
	}

	public class CharAttributeType {
		public const int HP = 0;
		public const int HPMax = 1;
		public const int HPReg = 2;
		public const int Attack = 3;
		public const int Defense = 4;
		public const int Count = 5;
	}

	public class EquipType {
		public const int Sword = 0;
		public const int Shield = 0;
		public const int Count = 2;
	}

	public class EquipPosition {
		public const int Head = 0;
		public const int Chest = 1;
		public const int Leg = 2;
		public const int Hand = 3;
		public const int Foot = 4;
		public const int Count = 5;
	}

	public class EquipDefinition {
		public string name;
		public ValueModifierSet mod;
	}
	public EquipDefinition[] equipDefinitions;

	public class BuffDefinition {
		public string name;
		public ValueModifierSet mod;
	}
	public BuffDefinition[] buffDefinitions;

	public class CharDefinition {
		public string name;
		public ValueSet[] levelAttributes;

	}
	public CharDefinition[] charDefinitions;

	public class SimpleChar {
		int type;
		int level;
		
		CharDefinition[] charDefinitions;
		EquipDefinition[] equipDefinitions;
		BuffDefinition[] buffDefinitions;

		ValueModifierSetArray modSet;
		ValueModifierSetArray equipModifierSet;
		ValueModifierSetArray buffModifierSet;


		ComplexValueSet attributes;

		
		public SimpleChar (CharDefinition[] charDefinitions, EquipDefinition[] equipDefinitions, 
		                   BuffDefinition[] buffDefinitions, 
		                   int typ, int lvl) {
			this.charDefinitions = charDefinitions;
			this.equipDefinitions = equipDefinitions;
			this.buffDefinitions = buffDefinitions;
			type = typ;
			level = lvl;

			modSet = new ValueModifierSetArray(CharAttributeType.Count, 2);
			equipModifierSet = new ValueModifierSetArray(CharAttributeType.Count, EquipPosition.Count);
			modSet.SetValueModifierSet(0, equipModifierSet);
			buffModifierSet = new ValueModifierSetArray(CharAttributeType.Count, 5);
			modSet.SetValueModifierSet(1, buffModifierSet);

			attributes = new ComplexValueSet(charDefinitions[type].levelAttributes[level], modSet);
		}

		public void SetLevel (int newLevel) {
			level = newLevel;
			UpdateBaseValue ();
		}

		public void SetEquip(int pos, int type) {
			equipModifierSet.SetValueModifierSet(pos, equipDefinitions[type].mod);
			// will auto call UpdateBaseValue () via events.
		}

		public void SetBuff (int pos, int buffType) {
			buffModifierSet.SetValueModifierSet(pos, buffDefinitions[buffType].mod);
			// will auto call UpdateBaseValue () via events.
		}

		void UpdateBaseValue ()
		{
			attributes.SetBaseValueSet(charDefinitions[type].levelAttributes[level]);
		}
	}

	public void SetupConfig () {
		charDefinitions = new CharDefinition[] {
			new CharDefinition () {
				name = "Fighter",
				levelAttributes = new ValueSet[maxLevelCount] {
					///HP, HPMax, HPReg, ATK, DEF, 
					new SimpleValueSet(new float[] {5,5,5,5,5}),
					new SimpleValueSet(new float[] {10,10,10,10,10}),
					new SimpleValueSet(new float[] {20,20,20,20,20})
					}
			},
			new CharDefinition () {
				name = "Mage",
				levelAttributes = new ValueSet[maxLevelCount] {
					new SimpleValueSet(new float[] {2,2,1,11,1}),
					new SimpleValueSet(new float[] {6,6,2,22,1}),
					new SimpleValueSet(new float[] {8,8,3,33,1})
				}
			},
		};

		equipDefinitions = new EquipDefinition[] {
			new EquipDefinition () {
				name = "Sword",
				mod = new ValueModifierSetFixed (
					CharAttributeType.Count, 
					new ValueModifier[] {
						new ValueModifier () {
							indexer = CharAttributeType.Attack,
							addend = 10,
							multiplier = 1,
						}
					}
				),
			}
		};
	}

	public void CharUpgrade () {
		SimpleChar fighter = new SimpleChar(charDefinitions, equipDefinitions, buffDefinitions, CharType.Fighter, 0);
		SimpleChar mage = new SimpleChar(charDefinitions, equipDefinitions, buffDefinitions, CharType.Mage, 0);

		fighter.SetLevel(1);
	}
	
	public static bool TestCase () {
		
		
		
		
//		ValueModifierSetFixed swordBonus = new ValueModifierSetFixed () {
//			modifierList = new ValueModifier[] {
//				new ValueModifier {
//					indexer = CharAttribute.Attack,
//					addend = 100,
//					multiplier = 1
//				},
//			},
//		};
//		
//		FixedValueModifierSet sheldBonus = new FixedValueModifierSet () {
//			modifierList = new ValueModifier[] {
//				new ValueModifier {
//					indexer = CharAttribute.Defense,
//					addend = 23,
//					multiplier = 1
//				},
//				
//				new ValueModifier {
//					indexer = CharAttribute.HPMax,
//					addend = 23,
//					multiplier = 1
//				},
//			},
//		};
//		
//		FixedValueModifierSet ringBonus = new FixedValueModifierSet () {
//			modifierList = new ValueModifier[] {
//				new ValueModifier {
//					indexer = CharAttribute.HPReg,
//					addend = 2,
//					multiplier = 1
//				},
//			},
//		};
		return true;
		
	}
}

