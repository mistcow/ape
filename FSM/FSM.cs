﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace APE {

	public delegate void Action ();
	public delegate bool Condition ();

	public class Transition {
		public Condition condition;
		public int nextState;
	}

	public class FSM {
		public Dictionary <int, FSMState> stateDict;
		private int currentStateID;
		private FSMState currentState { get { return stateDict[currentStateID];}}

		public FSM () {
			stateDict = new Dictionary <int, FSMState> ();
		}

		public void AddState (int stateID, FSMState state) {
			stateDict [stateID] = state;
			state.SetOwner (this);
		}

		public void ChangeState (int newStateID) {
			if (newStateID != currentStateID) {
				stateDict[currentStateID].exitAction();
				currentStateID = newStateID;
				stateDict[currentStateID].enterAction();
			}
		}

		public void Start (int initialStateID) {
			currentStateID = initialStateID;
			stateDict[currentStateID].enterAction();
		}

		public void Update () {
			foreach (Transition trans in currentState.TransitionList) {
				if (trans.condition ()) {
					ChangeState (trans.nextState);
					break;
				}
			}

			currentState.updateAction ();
		}
	}

	public class FSMState {
		public Action EmptyAction = delegate {};

		public FSM owner;
		public Action enterAction;
		public Action exitAction;
		public Action updateAction;

		public List <Transition> TransitionList;

		public FSMState (Action updateAction = null, Action enterAction = null, Action exitAction = null) {
			this.enterAction = enterAction == null ? EmptyAction : enterAction;
			this.exitAction = enterAction == null ? EmptyAction : exitAction;
			this.updateAction = enterAction == null ? EmptyAction : updateAction;

			TransitionList = new List<Transition> ();
		}

		public void AddTransition (Condition cond, int transitToState) {
			TransitionList.Add (new Transition {condition = cond, nextState = transitToState});
		}

		internal void SetOwner (FSM fsm) {
			owner = fsm;
		}
	}
}